/**
 * Here are some various methods with different Big O runtime efficiencies.
 * Remember, Big O notation not interested in constant factors -- only how
 * the number of operations increase with respect to the input size.
 */
public class BigODemo {
    
    /**
     * Helper method. Uses Thread.sleep to simulate "thinking" for 1 millisecond.
     */
    private static void calculateFor1Millisecond(){
        try {
            //Pause the program for 1 ms to simulate thinking.
            Thread.sleep(1);
        } catch (InterruptedException e) {}
    }
    
    /**
     * Helper method. Uses Thread.sleep to simulate "thinking" for 10 seconds.
     */
    private static void calculateFor10Seconds(){
        try {
            //Pause the program for 10 seconds to simulate thinking.
            Thread.sleep(10000);
        } catch (InterruptedException e) {}
    }
    
    
    /**
     * This method runs in O(1) (constant)-- it is NOT dependent on the size
     * of the input. Examples include getting the size of the array (which Java
     * stores as an instance variable, so we can fetch it quickly).
     */
    public static void methodA(Object[] input){
        calculateFor1Millisecond();
    }
    
    /**
     * This method runs in O(1) time -- it is again not dependent on the size of
     * the input. Even though it takes 10000 times longer to run than methodA above,
     * there is still no dependency on the size of the input array.
     */
    public static void methodB(Object[] input){
        calculateFor10Seconds();
    }
    
    /**
     * This method runs in O(n) (linear) time -- it is performing some operation
     * for each element of the list.
     */
    public static void methodC(Object[] input){
        for (int i = 0; i < input.length; i++){
            calculateFor1Millisecond();
        }
    }
    
    /**
     * This method also runs in O(n) time -- it is performing some operation for
     * each element of the list. It will take much longer than methodC above given
     * the same input, but linearly so by a constant factor of 10000.
     */
    public static void methodD(Object[] input){
        for (int i = 0; i < input.length; i++){
            calculateFor10Seconds();
        }
    }
    
    /**
     * Again, this method also runs in O(n) -- it is performing some operation
     * for each element in the list twice. So it will take twice as long as 
     * methodC for the same input -- a factor of 2 that we don't care about.
     */
    public static void methodE(Object[] input){
        for (int i = 0; i < input.length; i++){
            calculateFor1Millisecond();
        }
        for (int i = 0; i < input.length; i++){
            calculateFor1Millisecond();
        }
    }
    
    /**
     * Again, this method also runs in O(n) time. We are processing half of the
     * items in the list. It will run twice as fast as methodC since we are calling
     * our compute operation n/2 times. But 1/2 is still a constant factor.
     */
    public static void methodF(Object[] input){
        for (int i = 0; i < input.length / 2; i++){
            calculateFor1Millisecond();
        }
    }
    
    /**
     * This method runs in O(n^2) time (quadratic). We are processing each
     * element in the list for each element in our list. So if we have
     * 10 elements, that's 10 * 10 = 100 operations. Quadratic algorithms
     * grow much faster (and are therefore usually much slower) than linear ones
     * with respect to the input size. For example, with a large enough n, this
     * method will take longer to run than even methodD, our 10-second linear method.
     */
    public static void methodG(Object[] input){
        for (int i = 0; i < input.length; i++){
            for (int j = 0; j < input.length; j++){
                calculateFor1Millisecond();
            }
        }
    }
    
    /**
     * This method also runs in O(n^2). We are processing each element in the
     * list. For each element, we do something for each element previous in the
     * list. So we do [1 + 2 + 3 + ... + (n-1) + n] operations for a total
     * of (1/2)*n^2 + (1/2)*n operations. We can ignore the constant factors
     * to get n^2 + n operations. Now the n^2 part, as we showed in methodG, is
     * the DOMINATING term. The n term is not significant since it is so much
     * smaller than n^2. So we can also drop the n term and say this method
     * runs in O(n^2) time.
     */
    public static void methodH(Object[] input){
        for (int i = 0; i < input.length; i++){
            for (int j = 0; j <= i; j++){
                calculateFor1Millisecond();
            }
        }
    }
    
    /**
     * This method runs in O(n^3) time (cubic). Here we have a triply-nested
     * for loop going across all elements. So for 10 elements we would have to
     * perform 10 * 10 * 10 = 1000 operations. This runtime grows even faster
     * than O(n^2) so it is very costly.
     */
    public static void methodI(Object[] input){
        for (int i = 0; i < input.length; i++){
            for (int j = 0; j < input.length; j++){
                for (int k = 0; k < input.length; k++){
                    calculateFor1Millisecond();
                }
            }
        }
    }
    
    /**
     * This method runs in O(2^n) (exponential) time. This is one of the
     * slowest algorithms we have and grows faster than any O(n^c) polynomial-
     * time method, where c is some constant.
     * 
     * Examples include iterating through every subset of a list (e.g. given
     * the size-2 set {1,2} we have to iterate over {{}, {1}, {2}, {1,2}}
     * = 2^2 = 4 elements).
     */
    public static void methodJ(Object[] input){
        final int iterations = (int)Math.pow(2,input.length);
        for (int i = 0; i < iterations; i++){
            calculateFor1Millisecond();
        }
    }
    
    /**
     * This method runs in O(n!) (factorial) time. This is even slower than
     * an exponential if you can believe it. In this example, we are iterating
     * n! number of times (plus a negligible n number of times to compute
     * the actual factorial).
     * 
     * Examples of this include iterating over every possibly permutation
     * of a set (e.g. {1,2,3} requires iterating over {1,2,3}, {1,3,2}, {2,1,3},
     * {2,3,1}, {3,1,2}, and {3,2,1} = 3! = 6 different permutations).
     */
    public static void methodK(Object[] input){
        //First compute n! -- which takes O(n) time.
        int iterations = 1;
        for (int i = 1; i <= input.length; i++){
            iterations *= i;
        }
        //Now iterate that many times.
        for (int i = 0; i < iterations; i++){
            calculateFor1Millisecond();
        }
    }
    
    /**
     * This method runs in O(lg(n)) (logarithmic) time -- that is, log-base-2
     * of n. Here upon each iteration, we are decreasing our loop variable
     * by half. So the number of operations is proportional to how many times
     * we can divide the input size by 2 and still have a positive number, or
     * equivalently, how many times can we double 1 before we arrive at n.
     * That number is exactly log-base-2 of n. Logarithmic methods grow slower
     * (are faster) than linear O(n) time but of course grow faster (are slower)
     * than O(1) methods (which shouldn't be growing at all).
     * 
     * Examples of this include binarySearch which we saw in class.
     */
    public static void methodL(Object[] input){
        for (int i = input.length; i > 1; i /= 2){
            calculateFor1Millisecond();
        }
    }
    
    /**
     * This method runs in O(n*lg(n)) (loglinear) time. We are nesting the
     * logarithmic loop from above around an O(n) linear loop. This is going
     * to be even slower (grows faster) than a linear algorithm but still not
     * as slow as an O(n^2) method, which grows even faster.
     * 
     * Examples of this include the mergeSort algorithm which we will
     * show in class.
     */
    public static void methodM(Object[] input){
        for (int i = input.length; i > 1; i /= 2){
            for (int j = 0; j < input.length; j++){
                calculateFor1Millisecond();
            }
        }
    }

}