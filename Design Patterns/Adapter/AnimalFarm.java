public class AnimalFarm {
    
    public static void main(String[] args){
        
        Animal[] animalList = new Animal[]{
            new Owl(),
            new Cat()
            
        };
        for (int i = 0; i < animalList.length; i++){
            Animal thisAnimal = animalList[i];
            thisAnimal.speak();
        }
        
        
    }

}