/**
 * SuperCatClass is a top-of-the-line Cat implementation
 * and the leading choice for all your cat-modeling applications.
 * The product of 29 years of Cat-based R&D by Meowcrosoft, accept no
 * substitute for SuperCatClass.
 * 
 * This software is provided free for personal or commercial use but
 * does not come with any warranty.
 */
public class SuperCatClass {
    
    private int numberOfLivesRemaining;
    private int ap;
    private int hp;
    private int mp;

    public SuperCatClass(){
        this.numberOfLivesRemaining = 9;
        this.ap = 10;
        this.hp = 100;
        this.mp = 100;
    }
    
    public void attack(SuperCatClass other){
        other.hp -= this.ap;
    }
    
    public String getNoise(){
        return "meow";
    }
    
    public String getAnimalName(){
        return "cat";
    }
    
    public int getNumberOfLivesRemaining(){
        return this.numberOfLivesRemaining;
    }
    
    public int getAP(){
        return this.ap;
    }
    
    public int getHP(){
        return this.hp;
    }
    
    public int getMP(){
        return this.mp;
    }

}