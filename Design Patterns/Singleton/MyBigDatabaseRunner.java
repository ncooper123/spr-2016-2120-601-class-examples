/**
 * A demo runner for the Singleton pattern.
 * 
 * Here we show off how we would go about getting an instance of MyBigDatabase.
 * Since we made the constructor off limits, we have to go through the static
 * method we exposed. So both db and db2 refer to the same object.
 */
public class MyBigDatabaseRunner {
    
    public static void main(String[] args){
        
        
        MyBigDatabase db = MyBigDatabase.getInstance();
        
        db.getValue(6);
        db.getValue(8);
        
        MyBigDatabase db2 = MyBigDatabase.getInstance();
        db2.getValue(6);
        db2.getValue(8);
        
    }
    
    
}