/**
 * Here is an example of the singleton pattern.
 * 
 * This class is modeling a very large database. Creating new instances
 * of this object can take up a lot of memory, so we want to make sure that
 * only one object can ever exist.
 */
public class MyBigDatabase {
    
    /**
     * We start by making a static instance of our MyBigDatabase class.
     * This will be the only instance that ever exists.
     */
    private static MyBigDatabase instance;
    
    /**
     * Next, we expose some static method that lets use get that instance.
     * 
     * Why should this method and the variable above be static?
     */
    public static MyBigDatabase getInstance(){
        if (MyBigDatabase.instance == null){
            //The instance doesn't exist yet. Create it.
            instance = new MyBigDatabase();
        }
        return MyBigDatabase.instance;
    }
    
    //Instance variables for the MyBigDatabase class...
    private int[] db;
    // etc.

    /**
     * Now, we make the constructor private. When we change it to private,
     * we remove the ability for code elsewhere to call it directly and thus
     * make new instances.
     */
    private MyBigDatabase(){
        //Do whatever steps we need to setup a MyBigDatabase...
        this.db = new int[10000];
    }
    
    //Instance methods for the MyBigDatabase class...
    
    public int getValue(int x){
        if (x < 0 || x >= 10000){
            throw new IllegalArgumentException("Bad index.");
        }
        return this.db[x];
    }

}