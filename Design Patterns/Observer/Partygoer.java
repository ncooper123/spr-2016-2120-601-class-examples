import java.util.Observable;
import java.util.Observer;

/**
 * A Partygoer is a rad dude that watches the NYE countdown.
 * 
 * @author Nathan Cooper
 * @see Countdown
 */
public class Partygoer implements Observer {

    private String name;
    
    /**
     * The age of this Partygoer.
     */
    public int age = 30;
    
    /**
     * Constructs a new Partygoer with a given name.
     * 
     * @param name The desired name.
     */
    public Partygoer(String name){
        this.name = name;
    }
    
    /**
     * Returns the name of this <code>Partygoer</code>.
     * 
     * @return The name of this Partygoer.
     */
    public String getName(){
        return this.name;
    }
    
    /**
     * Prints out to standard output what this Partygoer says
     * in response to a timer integer.
     * 
     * @param timer The countdown timer value we are looking at.
     * @throws IllegalArgumentException When the timer value is negative.
     */
    public void shout(int timer){
        if (timer < 0){
            throw new IllegalArgumentException();
        }
        else if (timer == 0){
            System.out.println(this.name + ": Happy new year!");
        }
        else {
            System.out.println(this.name + ": " + timer);
        }
    }
    
    /**
     * {@inheritDoc}
     * 
     * Causes the shout method to be called.
     */
    @Override
    public void update(Observable obs, Object arg){
        Countdown countdown = (Countdown)obs;
        int timer = (Integer)arg;
        shout(timer);
    }
    

}