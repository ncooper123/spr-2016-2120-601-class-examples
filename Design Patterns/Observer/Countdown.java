import java.util.Observable;

public class Countdown extends Observable {

    private int timer;
    
    public Countdown(int startTime) {
        if (startTime <= 0){
            throw new IllegalArgumentException("Bad start time.");
        }
        this.timer = startTime;
    }
    
    public void step(){
        if (this.timer <= 0){
            throw new RuntimeException("Already at zero.");
        }
        this.timer--;
        this.setChanged();
        this.notifyObservers(this.timer);
    }
    

}