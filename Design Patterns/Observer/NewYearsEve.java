

public class NewYearsEve {

    public static void main(String[] args){
    
        Countdown countdown = new Countdown(10);
        
        Partygoer tommy = new Partygoer("Tommmy");
        countdown.addObserver(tommy);
        Partygoer jerry = new Partygoer("Jerry");
        countdown.addObserver(jerry);
        Partygoer anthony = new Partygoer("Anthony");
        countdown.addObserver(anthony);
        
        countdown.step();
        countdown.step();
        countdown.step();
        countdown.step();
        countdown.step();
        countdown.step();
        countdown.step();
        countdown.step();
        countdown.step();
        countdown.step();
    
    
    }

}