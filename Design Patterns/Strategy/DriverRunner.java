/**
 * A demo of the strategy pattern at work.
 * 
 * We create two different drivers with two different strategies and
 * set up some examples Drivers with them.
 */ 
public class DriverRunner {

    public static void main(String[] args){
    
        //First, we must instantiate the algorithms...
        DrivingStrategy timidStrategy = new TimidStrategy();
        DrivingStrategy aggressiveStrategy = new AggressiveStrategy();
        
        //Next, let's create two drivers.
        Driver jimmy = new Driver("Jimmy","John",timidStrategy);
        Driver betty = new Driver("Betty","Crocker",aggressiveStrategy);
        
        //Call their drive methods, which get delegated to the DrivingStrategy class.
        jimmy.drive();
        betty.drive();
        
        //And look! We can change a strategy at any given time.
        betty.setDrivingStrategy(timidStrategy);
        betty.drive();
    
    }



}