/**
 * The interface for our driving algorithm. This is the 
 * strategy that subclasses will have to implement.
 */
public interface DrivingStrategy {

    public void drive();

}