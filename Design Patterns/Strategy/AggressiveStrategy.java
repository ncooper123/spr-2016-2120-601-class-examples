/**
 * An aggressive implementation of driving.
 */ 
public class AggressiveStrategy implements DrivingStrategy {

    @Override
    public void drive(){
        System.out.println("move, get out the way");
    }

}