/**
 * A timid implementation of driving.
 */
public class TimidStrategy implements DrivingStrategy {

    @Override
    public void drive(){
        System.out.println("Oh golly gee... better slow down.");
    }

}