/**
 * A demonstraction of the Strategy pattern.
 * 
 * Here we have a Driver with a first name and last name. We want to have
 * different drivers drive differently when requested. How do we go about doing
 * this?
 * 
 * Well, we could subclass Driver directly, but then we wouldn't be able to
 * change how they drive.
 * 
 * Instead, let's move the driving algorithm out to its own class and call
 * it DrivingStrategy. Then we'll give each Driver a DrivingStrategy instance.
 * Now we can treat its strategy like any other instance variable and get/set
 * it at any given time.
 */
public class Driver {

    //Instance variables for Driver...
    private String firstName;
    private String lastName;
    
    //A reference to the DrivingStrategy we made...
    private DrivingStrategy drivingStrategy;
    
    /**
     * Constructor for a Driver. Note how we are instantiating it with
     * a default DrivingStrategy.
     */
    public Driver(String firstName, String lastName, DrivingStrategy drivingStrategy){
        this.firstName = firstName;
        this.lastName = lastName;
        this.drivingStrategy = drivingStrategy;
    }
    
    /**
     * If we want to be able to call drive() directly on a Driver object,
     * we can simply make a method here that performs the drive() method on
     * whatever drivingStrategy is set to.
     */
    public void drive(){
        this.drivingStrategy.drive();
    }
    
    /**
     * Finally, one of the benefits of the strategy pattern is the ability to
     * swap out the strategy at any time.
     */
    public void setDrivingStrategy(DrivingStrategy drivingStrategy){
        this.drivingStrategy = drivingStrategy;
    }

}