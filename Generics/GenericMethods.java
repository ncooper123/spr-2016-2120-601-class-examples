import java.util.List;
import java.util.ArrayList;

/**
 * This class shows some examples of generic methods.
 */
public class GenericMethods {

    /**
     * A simple generic method. It declares a generic type T and uses it as its
     * parameter type. The method itself just prints out a String version of t.
     * We are placing no bound on T, so T will be ERASED into Object at runtime.
     * We are fine calling .toString() then on t since all Objects have that
     * method.
     */
    public static <T> void methodA(T t){
        System.out.println(t.toString());
    }
    
    /**
     * Now we are placing an UPPER BOUND on T using Number. So we know T
     * is-a Number. Therefore, we can now call the intValue() method on
     * t, a method declared in the Number interface. We couldn't do this
     * in the above method without explicit casting.
     * 
     * If we wanted, we could use the "super" keyword to place a LOWER BOUND
     * on T.
     */
    public static <T extends Number> void methodB(T t){
        System.out.println(t.intValue());
    }
    
    /**
     * Now we have a non-generic method that takes a List<Number> as a parameter.
     * That is, any List<Number> or sub-type of List<Number> like ArrayList<Number>
     * can be passed in. We show this by adding a Float value into the list.
     */
    public static void methodC(List<Number> list){
        list.add(12.8);
    }
    
    /**
     * ... But let's look deeper into this...
     */
    public static void testMethodC(){
        //This works. We are passing in a List of numbers.
        List<Number> list1 = java.util.Arrays.asList(new Number[]{1,2,23.3,8});
        methodC(list1);
        
        /*
         * But this won't work -- we'd be passing in a List<Integer> which
         * is-not-a List<Number>. This makes sense -- would methodC be able
         * to add that float value into a strictly-Integer list? No.
         */
        /*
        List<Integer> list2 = java.util.Arrays.asList(new Integer[]{1,2,2,8});
        methodC(list2);
        */
    }
    
    /**
     * Now we can use a generic method to declare a type T with an upper bound
     * of Number. And accept a parameter of a List of that type. As an example,
     * we can use the List method get() to get the first value and then use
     * the intValue() method in Number to print out the first value's integer
     * representation.
     */
    public static <T extends Number> void methodD(List<T> list){
        System.out.println(list.get(0).intValue());
    }
    
    /**
     * Now, both versions are valid. The type parameter of List is being
     * generalized.
     */
    public static void testMethodD(){
        List<Number> list1 = java.util.Arrays.asList(new Number[]{1,2,23.3,8});
        methodD(list1);
        
        List<Integer> list2 = java.util.Arrays.asList(new Integer[]{1,2,2,8});
        methodD(list2);
    }
    
    /**
     * And if methodD was too verbose for you, we can re-write it using
     * the ? wildcard operator.
     */
    public static void methodE(List<? extends Number> list){
        System.out.println(list.get(0).intValue());
    }
}