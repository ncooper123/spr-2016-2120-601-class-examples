import java.util.Scanner;

/**
 * A very simple generic class that simply wraps a Number instance variable
 * of some type.
 * 
 * The part in <...> signifies that this is a generic class -- one that will
 * be making use of some generic type defined later upon instantiation.
 * 
 * The extends part is placing an UPPER BOUND on the type of T, meaning that
 * whatever type T is must be a subclass of Number.
 */
public class MyValue<T extends Number> {

    /**
     * Our current value of type T.
     */
    private T value;
    
    /**
     * Constructs a new MyValue with a given value.
     * 
     * @param T The initial value.
     */
    public MyValue(T initialValue){
        this.value = initialValue;
    }
    
    /**
     * Returns the current value. Note that the return type is T.
     * 
     * @return The current value.
     */
    public T getValue(){
        return this.value;
    }
    
    /**
     * Sets the current value. Note that the argument type is T.
     * 
     * @param value The new value to set.
     */
    public void setValue(T value){
        this.value = value;
    }
    
    /**
     * Now let's test it out.
     * 
     * @param args Unused.
     */
    public static void main(String[] args){
        
        //This works, since the Integer wrapper class is a Number.
        MyValue<Integer> n = new MyValue<>(12);
        
        //So does this.
        MyValue<Double> d = new MyValue<>(12.0);
        
        //But this wouldn't work. Strings are not Numbers.
        /*
        MyValue<String> s = new MyValue<>("Hello");
        */
        
    }

}