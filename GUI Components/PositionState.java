/**
 * An enum representing either an X, O, or empty square.
 */
public enum PositionState {
	
	X,
	O,
	EMPTY

}