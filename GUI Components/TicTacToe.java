import java.util.Observer;
import java.util.Observable;

/**
 * A class that models a game of Tic-Tac-Toe.
 *
 * This is the MODEL part of our MVC application. Here we are
 * concerned with only representing the state of the game and providing
 * the logic for playing it. We are NOT interested in any kind of
 * visual/display information.
 *
 * We extend Observable so that we can notify our observers (Views)
 * anytime we create a change.
 */
public class TicTacToe extends Observable {
	
	/** The state of the board, from top-left to bottom-right. */
	private PositionState[] board;

	/** The current turn number. We will also use it to encode who's turn,
	  * (X or O) it is. */
	private int turn;

	/**
	 * Constructs a new empty Tic-Tac-Toe game.
	 */
	public TicTacToe(){
		this.board = new PositionState[9];
		java.util.Arrays.fill(this.board,PositionState.EMPTY);
		this.turn = 1;
	}

	/**
	 * Advances the game one turn by moving to a given position.
	 *
	 * @param position The position to move.
	 */
	public void takeTurn(final int position){
		if (this.board[position] != PositionState.EMPTY){
			//Don't do anything if we've already moved here.
			throw new IllegalArgumentException("Position already filled.");
		}
		//Add the current player's symbol to the position and advance the turn.
		this.board[position] = (this.turn % 2 == 0) ? PositionState.X : PositionState.O;
		this.turn++;
		//Notify our observers of the change, passing the position changed.
		this.setChanged();
		this.notifyObservers(position);
	}

	/**
	 * A getter method for returning the PositionState at a given position.
	 *
	 * @param position The position (0-8) to request.
	 * @return The PositionState of the requested position.
	 */
	public PositionState getPositionState(final int position){
		return this.board[position];
	}


}