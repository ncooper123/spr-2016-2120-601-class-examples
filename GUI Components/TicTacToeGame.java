/**
 * Our launcher for our TicTacToe application. Runs from main().
 */
public class TicTacToeGame {
	
	/**
	 * Sets up a Swing Tic-Tac-Toe game and runs it.
	 *
	 * @param args Command-line arguments. Unused.
	 */
	public static void main(String[] args){
		//Setup the model and the view.
		TicTacToe game = new TicTacToe();
		TicTacToeViewer viewer = new TicTacToeViewer(game);

		//Register the view to the model.
		game.addObserver(viewer);

		//Tell the program to exit when we click the close button.
		viewer.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);

		//Set a default size of 500-by-500 pixels.
		viewer.setSize(500,500);

		//Set the JFrame to visible, thereby beginning the game.
		viewer.setVisible(true);
	}

}