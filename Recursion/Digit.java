/**
 * A base digit. For each operation, it will also
 * act upon its associate if necessary.
 */
public class Digit implements VirtualDigit {

    /** The current value of this Digit. @invariant (value <= 9 && value >= 0) */
    private int value;
    
    /** The VirtualDigit to this digit's left. */
    private VirtualDigit associate;
    
    /**
     * Creates a new Digit with a given associate.
     * 
     * @param associate. The digit to this digit's left.
     * @param value. The current value.
     */
    public Digit(VirtualDigit associate){
        this.associate = associate;
        this.value = 0;
    }
    
    /**
     * {@InheritDoc}
     */
    @Override
    public void increment(){
        this.value = (this.value + 1) % 10;
        if (this.value == 0){
            this.associate.increment();
        }
    }
    
    /**
     * {@InheritDoc}
     */
    @Override
    public void reset(){
        this.value = 0;
        this.associate.reset();
    }
    
    /**
     * {@InheritDoc}
     */
    @Override
    public String toString(){
        return this.associate.toString() + Integer.toString(this.value);
    }

}