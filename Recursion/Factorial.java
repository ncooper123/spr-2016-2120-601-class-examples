/**
 * A simple example of recursion to compute the factorial of a number, n.
 * 
 * The factorial of a number, or n! is defined to be:
 * 
 * 0! = 1
 * n! = n(n-1)(n-2) ... (1)
 * 
 * Where the right-hand-side of the second definition can be re-written as:
 * 
 * n! = n(n-1)!
 * 
 * Let's use recursion to implement this definition.
 */
public class Factorial {

    /**
     * Computes n!
     * 
     * @param n The number we are computing the factorial of.
     */
    public static int factorial(int n){
        if (n <= 1){
            //n 1 or less. Just return 1 -- the base case.
            return 1;
        }
        else {
            //Use our definition above to recursively call factorial().
            //This is the general case.
            return n * factorial(n-1);
        }
    }
    
    /**
     * Prints 4! to standard output.
     */
    public static void main(String[] args){
        System.out.println(factorial(4));
    }

}