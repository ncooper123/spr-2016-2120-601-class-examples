import java.util.ArrayList;
import java.util.List;

/**
 * SOLUTION TO HW2. DO NOT DISTRIBUTE!!!
 */
public class RecursiveStrings {

    public static final int compareTo(String s1, String s2){
        s1 = s1.toLowerCase();
        s2 = s2.toLowerCase();
        if (s1.length() == 0 && s2.length() == 0){
            return 0;
        }
        else if (s1.length() == 0){
            return -1;
        }
        else if (s2.length() == 0){
            return 1;
        }
        else if (s1.charAt(0) != s2.charAt(0)){
            return s1.charAt(0) - s2.charAt(0);
        }
        else {
            return compareTo(s1.substring(1),s2.substring(1));
        }
    }
    
    public static final String findMinimum(ArrayList<String> list){
        return findMinimum(list.subList(1,list.size()),list.get(0));
    }
    
    private static final String findMinimum(List<String> list, String currentMin){
        if (list.size() == 0){
            //Base case. The list is empty.
            return currentMin;
        }
        else {
            //Recursive case. What is the minimum between currentMin and findMinimum(rest of list)?
            String minOfTail = findMinimum(list.subList(1,list.size()),list.get(0));
            if (compareTo(minOfTail,currentMin) < 0){
                return minOfTail;
            }
            else {
                return currentMin;
            }
        }
    }

}