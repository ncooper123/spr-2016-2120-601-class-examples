import java.nio.file.*;
import java.io.*;
import java.util.Scanner;
import java.util.NoSuchElementException;

public class CreateSequentialFile {

    private static ObjectOutputStream output;
    
    public static void main(String[] args){
        openFile();
        addRecords();
        closeFile();
    }
    
    public static void openFile(){
        try {
            output = new ObjectOutputStream(Files.newOutputStream(Paths.get("clients.ser")));
        }
        catch (IOException e){
            System.err.println("Error opening file. Terminating");
            System.exit(1);
        }
    }
    
    public static void addRecords(){
        Scanner input = new Scanner(System.in);
        System.out.printf("Enter account number, first name, last name, and balance.");
        System.out.println("Enter end-of-file indicator to end input.");
        System.out.print("INPUT: ");
        while (input.hasNext()){
            try {
                Account record = new Account(
                    input.nextInt(),
                    input.next(),
                    input.next(),
                    input.nextDouble()
                );
                output.writeObject(record);
            } catch (NoSuchElementException e){
                System.err.println("Invalid input. Please try again.");
                input.nextLine();
            } catch (IOException e){
                System.err.println("Error writing to file. Terminating.");
                break;
            }
            System.out.print("INPUT: ");
        }
    }
    
    public static void closeFile(){
        try {
            if (output != null){
                output.close();
            }
        } catch (IOException e){
            System.out.println("Error closing file. Terminating");
        }
    }

}