import java.io.IOException;
import java.lang.IllegalStateException;
import java.nio.file.Paths;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class CreditInquiry {
    
    private static final MenuOption[] choices = MenuOption.values();

    public static void main(String[] args){
        MenuOption accountType = getRequest();
        while (accountType != MenuOption.END){
            switch (accountType){
                case ZERO_BALANCE:
                    System.out.println("Accounts with zero balance:");
                    break;
                case CREDIT_BALANCE:
                    System.out.println("Accounts with credit balance:");
                    break;
                case DEBIT_BALANCE:
                    System.out.println("Accounts with debit balance:");
                    break;
            }
            readRecords(accountType);
            accountType = getRequest();
        }
    }
    
    private static MenuOption getRequest(){
        int request = 4;
        System.out.println("What do you want?\n1) Zero Balance Accounts\n2) Credit Accounts\n3) Debit Accounts\n4) Exit\n");
        try {
            Scanner input = new Scanner(System.in);
            do {
                System.out.print("Choice: ");
                request = input.nextInt();
            } while (request < 1 || request > 4);
        } catch (NoSuchElementException e){
            System.err.println("Invalid input. Terminating.");
        }
        return choices[request - 1];
    }
    
    private static void readRecords(MenuOption accountType){
        
        try (Scanner input = new Scanner(Paths.get("clients.txt"))){
            while (input.hasNext()){
                int accountNumber = input.nextInt();
                String firstName = input.next();
                String lastName = input.next();
                double balance = input.nextDouble();
                if (shouldDisplay(accountType,balance)){
                    System.out.printf(
                        "%-10d%-12s%-12s%12.2f%n",
                        accountNumber,
                        firstName,
                        lastName,
                        balance
                    );
                }
                else {
                    input.nextLine();
                }
            }
        } catch (NoSuchElementException | IOException | IllegalStateException e){
            System.err.println("Error handling file. Terminating.");
            System.exit(1);
        }
        
    }
    
    private static boolean shouldDisplay(MenuOption accountType, double balance){
        if (accountType == MenuOption.CREDIT_BALANCE && balance < 0){
            return true;
        }
        else if (accountType == MenuOption.DEBIT_BALANCE && balance > 0){
            return true;
        }
        else if (accountType == MenuOption.ZERO_BALANCE && balance == 0){
            return true;
        }
        else {
            return false;
        }
    }

}