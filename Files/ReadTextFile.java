import java.io.IOException;
import java.lang.IllegalStateException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class ReadTextFile {

    private static Scanner input;
    
    public static void main(String[] args){
        openFile();
        readRecords();
        closeFile();
    }
    
    public static void openFile(){
        try {
            input = new Scanner(Paths.get("clients.txt"));
        } catch (IOException e){
            System.err.println("Error opening file. Terminating.");
            System.exit(1);
        }
    }
    
    public static void readRecords(){
        System.out.printf(
            "%-10s%-12s%-12s%13s%n",
            "Account",
            "First Name",
            "Last Name",
            "Balance"
        );
        try {
            while (input.hasNext()){
                int accountNumber = input.nextInt();
                String firstName = input.next();
                String lastName = input.next();
                double balance = input.nextDouble();
                System.out.printf(
                    "%-10d%-12s%-12s%12.2f%n",
                    accountNumber,
                    firstName,
                    lastName,
                    balance
                );
            }
        } catch (NoSuchElementException e){
            System.err.println("File improperly formed. Terminating");
        } catch (IllegalStateException e){
            System.out.println("Error readindg from file. Terminating");
        }
    }
    
    public static void closeFile(){
        if (input != null){
            input.close();
        }
    }
    

}