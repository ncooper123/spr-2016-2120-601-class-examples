import java.net.*;
import java.io.*;

public class ChatConnection {

	private String name;
	private Socket socket;
	
	public ChatConnection(String name, Socket socket){
		this.name = name;
		this.socket = socket;		
	}
	
	public void start(){
		//Start listening in the background...
		(new Thread(new ChatListener())).start();
		
		//And then keep listening for keyboard information.
		String line = "";
		try {
			//Write to the socket.
			final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(this.socket.getOutputStream()));
			
			//Read from the keyboard.
			final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			
			//Write each line the user types.
			while ((line = reader.readLine()) != null){
				writer.write(this.name + ": " + line);
				writer.newLine();
				writer.flush();
			}
		} catch (IOException e){
			System.out.println(e.getMessage());
		}
	}
	
	private class ChatListener implements Runnable {
		
		//Continually read from the socket.
		@Override
		public void run(){
			String line = "";
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				while ((line = reader.readLine()) != null){
					System.out.println(line);
				}
			} catch (IOException e){
				System.out.println(e.getMessage());
			}
		}
	}

}