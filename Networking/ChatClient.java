import java.net.*;
import java.io.*;

public class ChatClient {
	
	public void start(String name, String server, int port){
		try {
			System.out.println("Connecting to " + server + ":" + port + "...");
			Socket socket = new Socket(server,port);
			ChatConnection connection = new ChatConnection(name,socket);
			connection.start();
		} catch (Exception e){
			System.out.println("Could not connect to server: " + e.getMessage());
		}
	}

   /**
	* Usage:
 	* java ChatServer <server-address> <port> <name>
	*/
	public static void main(String[] args){
		String address = args[0];
		int port = 0;
		try {
			port = Integer.parseInt(args[1]);
		} catch (Exception e){
			System.out.println("Invalid port: " + args[1]);
			System.exit(1);
		}
		String name = args[2];
		ChatClient client = new ChatClient();
		client.start(name,address,port);
	}

}