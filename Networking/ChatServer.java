import java.net.*;
import java.io.*;

public class ChatServer {
	
	private ServerSocket serverSocket;
	
	public void start(String name, int port){
		try {
			this.serverSocket = new ServerSocket(port);
		} catch (IOException e){
			System.out.println("Could not start server: " + e.getMessage());
			System.exit(1);
		}
		while (true){
			System.out.println("Awaiting a connection on port " + port + "...");
			try {
				Socket socket = this.serverSocket.accept();
				System.out.println("Connected to " + socket.toString());
				ChatConnection chatConnection = new ChatConnection(name,socket);
				chatConnection.start();
			} catch (IOException e){
				System.out.println(e.getMessage());
			}
		}
	}
	
	/**
	 * Usage:
	 *
	 * java ChatServer <port> <name>
	 */
	public static void main(String[] args){
		int port = 0;
		try {
			port = Integer.parseInt(args[0]);
		} catch (Exception e){
			System.out.println("Invalid port: " + args[0]);
			System.exit(1);
		}
		String name = args[1];
		ChatServer server = new ChatServer();
		server.start(name,port);
	}

}