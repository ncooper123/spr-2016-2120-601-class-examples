import java.util.concurrent.*;

public class SharedBufferTest{

    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newCachedThreadPool();
        Buffer sharedLocation = new UnsynchronizedBuffer();
        System.out.println("Action\t\tValue\tSum of Produced\tSum of Consumed");
        executorService.execute(new Producer(sharedLocation));
        executorService.execute(new Consumer(sharedLocation));
        executorService.shutdown();
        executorService.awaitTermination(1, TimeUnit.MINUTES);
    }

}