public class ArrayWriter implements Runnable {
    
    private final SimpleArray sharedSimpleArray;
    private final int startValue;
    
    public ArrayWriter(int value, SimpleArray sharedSimpleArray){
        this.startValue = value;
        this.sharedSimpleArray = sharedSimpleArray;
    }
    
    public void run(){
        for (int i = startValue; i < startValue + 3; i++){
            sharedSimpleArray.add(i);
        }
    }

}