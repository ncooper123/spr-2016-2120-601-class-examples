import java.security.SecureRandom;

public class SimpleArray {

    private static final SecureRandom SECURE_RANDOM = new SecureRandom();
    private final int[] array;
    private int writeIndex;
    
    public SimpleArray(int size){
        this.array = new int[size];
    }
    
    public void add(int value){
        int position = writeIndex;
        try {
            Thread.sleep(SECURE_RANDOM.nextInt(500));
        }
        catch (InterruptedException e){
            Thread.currentThread().interrupt();
        }
        this.array[position] = value;
        System.out.printf("%s wrote %2d to element %d.%n", Thread.currentThread().getName(),value,position);
        this.writeIndex++;
        System.out.printf("Next write index: %d%n",writeIndex);
    }
    
    @Override
    public String toString(){
        return java.util.Arrays.toString(this.array);
    }

}