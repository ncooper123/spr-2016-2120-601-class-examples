import java.security.SecureRandom;

public class Producer implements Runnable {

    private static final SecureRandom SECURE_RANDOM = new SecureRandom();
    private final Buffer sharedLocation;
    
    public Producer (Buffer sharedLocation){
        this.sharedLocation = sharedLocation;
    }
    
    public void run(){
        int sum = 0;
        for (int count = 1; count <= 10; count++){
            try {
                Thread.sleep(SECURE_RANDOM.nextInt(3000));
                sharedLocation.blockingPut(count);
                sum += count;
                System.out.printf("\t%2d%n",sum);
            } catch (InterruptedException e){
                Thread.currentThread().interrupt();
            }
        }
        System.out.println("Producer done producing.");
    }
    

}