import java.util.NoSuchElementException;

class ListNode<T> {

    T data;
    ListNode<T> nextNode;
    
    ListNode(T object){
        this(object,null);
    }
    
    ListNode(T data, ListNode<T> nextNode){
        this.data = data;
        this.nextNode = nextNode;
    }
    
    T getData(){
        return this.data;
    }
    
    ListNode<T> getNext(){
        return this.nextNode;
    }

}

public class List<T> {
    
    private ListNode<T> firstNode, lastNode;
    
    public List(){
        this.firstNode = this.lastNode = null;
    }
    
    public boolean isEmpty(){
        return this.firstNode == null;
    }
    
    public void insertAtFront(T data){
        if (this.isEmpty()){
            this.firstNode = this.lastNode = new ListNode<T>(data);
        }
        else {
            this.firstNode = new ListNode<T>(data,this.firstNode);
        }
    }
    
    public void insertAtBack(T data){
        if (this.isEmpty()){
            this.firstNode = this.lastNode = new ListNode<T>(data);
        }
        else {
            this.lastNode.nextNode = new ListNode<T>(data);
            this.lastNode = this.lastNode.nextNode;
        }
    }
    
    public T removeFromFront() {
        if (this.isEmpty()){
            throw new NoSuchElementException();
        }
        T removedItem = firstNode.data;
        if (firstNode == lastNode){
            this.firstNode = this.lastNode = null;
        }
        else {
            this.firstNode = this.firstNode.nextNode;
        }
        return removedItem;
    }
    
    public T removeFromBack() {
        if (this.isEmpty()){
            throw new NoSuchElementException();
        }
        T removedItem = lastNode.data;
        if (firstNode == lastNode){
            this.firstNode = this.lastNode = null;
        }
        else {
            ListNode<T> current = this.firstNode;
            while (current.nextNode != this.lastNode){
                current = current.nextNode;
            }
            this.lastNode = current;
            current.nextNode = null;
        }
        return removedItem;
    }
    
    public T get(int index){
        if (this.isEmpty()){
            throw new NoSuchElementException();
        }
        int i = 0;
        ListNode<T> current = this.firstNode;
        while (i < index){
            current = current.nextNode;
            i++;
            if (current == null){
                throw new NoSuchElementException();
            }
        }
        return current.data;
    }
    
    public static void main(String[] args){
        List<Integer> list = new List<>();
        list.insertAtBack(4);
        list.insertAtBack(5);
        list.insertAtBack(1);
        list.insertAtFront(9);
        System.out.println(list.get(4));
    }
    
}