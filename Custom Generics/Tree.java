class TreeNode<T extends Comparable<T>> {
    
    TreeNode<T> leftNode, rightNode;
    T data;
    
    public TreeNode(T data){
        this.data = data;
        this.leftNode = this.rightNode = null;
    }
    
    public void insert(T data){
        if (data.compareTo(this.data) < 0){
            //On left side of the tree.
            if (this.leftNode == null){
                this.leftNode = new TreeNode<T>(data);
            }
            else {
                this.leftNode.insert(data);
            }
        }
        else if (data.compareTo(this.data) > 0) {
            if (this.rightNode == null){
                this.rightNode = new TreeNode<T>(data);
            }
            else {
                this.rightNode.insert(data);
            }
        }
    }
    
    public boolean contains(T data){
        int compare = data.compareTo(this.data);
        if (compare == 0){
            return true;
        }
        else if (compare < 0){
            if (this.leftNode == null){
                return false;
            }
            else {
                return this.leftNode.contains(data);
            }
        }
        else {
            if (this.rightNode == null){
                return false;
            }
            else {
                return this.rightNode.contains(data);
            }
        }
    }
    
    public void traversePreorder(){
        System.out.println(this.data);
        if (this.leftNode != null){
            this.leftNode.traversePreorder();
        }
        if (this.rightNode != null){
            this.rightNode.traversePreorder();
        }
    }
    
    public void traversePostorder(){
        if (this.leftNode != null){
            this.leftNode.traversePreorder();
        }
        if (this.rightNode != null){
            this.rightNode.traversePreorder();
        }
        System.out.println(this.data);
    }
    
    public void traverseInOrder(){
        if (this.leftNode != null){
            this.leftNode.traverseInOrder();
        }
        System.out.println(this.data);
        if (this.rightNode != null){
            this.rightNode.traverseInOrder();
        }
    }

}

public class Tree<T extends Comparable<T>> {
    
    private TreeNode<T> root;
    
    public Tree(){
        this.root = null;
    }
    
    public void add(T data){
        if (this.root == null){
            this.root = new TreeNode<T>(data);
        }
        else {
            this.root.insert(data);
        }
    }
    
    public boolean contains(T data){
        if (this.root == null){
            return false;
        }
        else {
            return this.root.contains(data);
        }
    }
    
    public void traversePreorder(){
        if (this.root != null){
            this.root.traversePreorder();
        }
    }
    
    public void traversePostorder(){
        if (this.root != null){
            this.root.traversePostorder();
        }
    }
    
    public void traverseInOrder(){
        if (this.root != null){
            this.root.traverseInOrder();
        }
    }
    
    public static void main(String[] args){
        Tree<Integer> tree = new Tree<>();
        tree.add(5);
        tree.add(8);
        tree.add(17);
        tree.add(13);
        tree.add(0);
        tree.add(-1);
        tree.add(27);
        tree.traverseInOrder();
        System.out.println(tree.contains(8));
    }
    
}