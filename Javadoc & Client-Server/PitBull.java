/**
 * A possible subclass of Dog.
 */
public class PitBull extends Dog {

    /**
     * Constructs a new PitBull with a given name.
     * 
     * @param name The name of the PitBull.
     */
    public PitBull(String name){
        super(name);
    }

    /**
     * {@InheritDoc}
     * 
     * Here, we could either:
     * 
     * LOOSEN the allowable range of trainerVoice
     * or
     * STRENGTHEN the conditions on the return value.
     * 
     * See the write-up in the DogExample class for an explanation.
     */
    @Override
    public float bark(final float trainerVoice){
        return 50.0f;
    }
}