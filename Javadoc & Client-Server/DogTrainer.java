/**
 * Represents a DogTrainer object.
 */
public class DogTrainer {
    
    /** The name of the DogTrainer. */
    private String name;
    
    /** The DogTrainer's Dog. */
    private Dog dog;

    /**
     * Creates a new DogTrainer with a given name and Dog.
     * 
     * @param name The name of the DogTrainer.
     * @param dog The DogTrainer's dog.
     */
    public DogTrainer(String name, Dog dog){
        this.name = name;
        this.dog = dog;
    }
    
    /**
     * Invokes this DogTrainer's dog's bark() method.
     * 
     * Here, we are the client to bark(). We must make sure that the preconditions
     * are met. bark() requires a trainerVoice between 0 and 100. We will
     * check that here, since we don't know if the server will validate them.
     * Remember, if the preconditions are not met, the server promises nothing.
     */ 
    public float trainToBark(float trainerVoice){
        if (trainerVoice < 0 || trainerVoice > 100){
            throw new IllegalArgumentException("Bad trainer voice.");
        }
        return this.dog.bark(trainerVoice);
    }
    
    /**
     * Runner for the DogTrainer class. Trains a dog to bark.
     */
    public static void main(String[] args){
        Dog dog = new Dog("Cat");
        DogTrainer dogTrainer = new DogTrainer("Ash",dog);
        dogTrainer.trainToBark(-10.0f);
    }
    
}