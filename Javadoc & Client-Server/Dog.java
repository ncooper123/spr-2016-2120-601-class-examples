/**
 * A class that models a dog.
 * 
 * Here are are interested in the client-server contract. A special
 * relationship exists when a method is called.
 * 
 * The CLIENT is the method that is making the call.
 * The SERVER is the method that the client calls.
 * 
 * This relationship depends on who is calling what (i.e. a method can be
 * a server in one context and a client in another).
 * 
 * The client-server contract says:
 * 
 * The client will make sure it meets all the PRECONDITIONS that the server
 * requires. That is, it will make sure any parameters it gives the method
 * are valid and that the object it is calling the method on is in a valid
 * state to receive the call.
 * 
 * The server with make sure that all POSTCONDITIONS are met. That is, if the
 * preconditions are valid, it will make a guarantee about what return
 * value the method provides as well as its state upon method completion. It
 * must also ensure all INVARIANTS are met -- guarantees about the state of
 * the object and the value of any instance variables throughout the lifetime
 * of the object.
 * 
 * If the preconditions are not met, the server PROMISES nothing.
 */
public class Dog {

    /**
     * The name of the Dog.
     * 
     * @invariant name.length() <= 10000000
     */
    private String name;
    
    /**
     * Constructs a new Dog with a given name.
     * 
     * @param name The Dog's name.
     */
    public Dog(final String name){
        this.name = name;
    }
    
    /**
     * Returns the name of this Dog.
     * 
     * @return The name of the Dog.
     */
    public String getName(){
        return this.name;
    }
    
    /**
     * Returns the bark sound, in decibels, given a trainer's voice,
     * also in decibels. In our class example, this was the SERVER method,
     * since the DogTrainer class was calling it.
     * 
     * Typically we use this syntax to document parameters and return
     * values:
     * 
     * @param trainerVoice The trainer's voice, in decibels. Must be strictly between 0 and 100.
     * @return The bark sound's volume in decibels. Guaranteed to be between 0 and 200.
     * 
     * We can also use this notation to document pre- and post conditions
     * explicitly:
     * 
     * @require trainerVoice <= 100.0 && trainerVoice >= 0.0
     * @ensure bark() <= 200.0 && bark() >= 0.0
     */
    public float bark(final float trainerVoice){
        //Return a random value. Use your imagination here.
        return 75.0f;
    }



}