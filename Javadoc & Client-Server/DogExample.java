/**
 * An example of why it's important to pay attention to the client-server
 * contract whenever we create subclasses.
 */
public class DogExample {

    /**
     * Main method. Just calls the start() method.
     * 
     * @param args The command-line arguments. Ignored.
     */
    public static void main(String[] args){
        start();
    }
    
    /**
     * Runs an example of how contracts work with subclasses.
     * 
     * Here we create an array of Dog objects called dogs. According to
     * the is-a relationship of inheritance, any object in the array can
     * be either:
     * 
     * 1) A Dog object, or 
     * 2) A subclass of Dog.
     * 
     * So when we call bark() on any one of these, we are making a polymorphic
     * call to either the bark() method in Dog or a possible Override
     * in the subclass.
     * 
     * Looking at the Dog class's contract in bark(), we see that the
     * trainerVoice must be between 0 and 100 and that the return value
     * can range from 0 and 200.
     * 
     * Now imagine we have a subclass of Dog, called Pitbull. What happens
     * if the PitBul class makes changes to any of the bark() method's
     * contracts?
     * 
     * A) The allowable values of trainerVoice are extended: This is OK. We
     * are allowing new parameters in the subclass, but it's not going to
     * break any of the code below, since we are making sure to only call
     * bark with values between 0 and 100. LOOSENING PRECONDITIONS IS OK.
     * 
     * B) The allowable values of trainerVoice are restricted: This is bad.
     * If PitBull suddenly makes some values of trainerVoice disallowed,
     * then code here can break. For instance, if 25.0f is no longer valid,
     * then the code will break. STRENGTHENING PRECONDITIONS IS BAD.
     * 
     * C) The range of return values is restricted: This is OK. If the bark
     * return value in PitBull just happens to be a subset of the total allowable
     * range defined in Dog's contract that is fine. RESTRICTING POSTCONDITIONS
     * IS OK.
     * 
     * D) The range of return values is exended. This is bad. If PitBull can
     * suddenly return values betwen, say, -100 and 200, our code below
     * might not know how to handle those cases, since based on Dog's contract,
     * it is only preparing for values between 0 and 200. LOOSENING POSTCONDITIONS
     * IS BAD.
     */
    private static void start(){
        
        Dog[] dogs = new Dog[]{
            new PitBull("Snoop"),
            new Dog("Lion"),
            new Dog("Loser")
        };
        
        for (Dog dog : dogs){
            float response = dog.bark(25.0f);
            System.out.println(dog.getName() + " responded at " + response + "dB.");
            if (response > 200.0f){
                /*
                 * Well Dog says, the value will never be greater than 200,
                 * so I don't see why we shouldn't just launch these nukes
                 * if the response is ever greater...
                 */
                throw new RuntimeException("Unplanned exception. Launching missles.");
            }
        }
        
        
    }
    
    

}