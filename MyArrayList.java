/**
 * TEST SOLUTION: DO NOT DISTRIBUTE!!
 * 
 * A class representing a list, similar to the
 * java.util.ArrayList class from the Java SDK. Keeps track
 * of a list of integers. Can only hold a maximum number of
 * values (i.e. we do not automatically adjust the size if we
 * grow too big).
 */
public class MyArrayList {

    /** The number of elements currently in the list. */
    private int size;
    
    /** The internal array storage of the list. */
    private int[] elements;
    
    /**
     * Constructs a new MyArrayList with a given maximum size.
     * 
     * @param maxSize The maximum values we can hold.
     */
    public MyArrayList(final int maxSize){
        this.size = 0;
        this.elements = new int[maxSize];
    }
    
    /**
     * Appends a new integer value to this MyArrayList.
     * 
     * @param n The value to add to the end.
     * @throws RuntimeException If the MyArrayList is already full.
     */
    public void add(final int n){
        if (this.size >= this.elements.length){
            throw new RuntimeException("MyArrayList is full!");
        }
        this.elements[this.size] = n;
        this.size++;
    }
    
    /**
     * Returns the integer element at the specified index.
     * 
     * @param index The index of the desired value. Assumed to be between 0 and this.size.
     * @return The elemenet at the specified value.
     */
    public int get(final int index){
        return this.elements[index];
    }
    
    /**
     * Removes the element from the specified index, shifting all values
     * after it down to fill the missing place.
     * 
     * @param index The index to remove. Assumed to be between 0 and this.size.
     */
    public void remove(final int index){
        for (int i = index; i < this.size-1; i++){
            this.elements[i] = this.elements[i+1];
        }
        this.size--;
        this.elements[this.size] = -1;
    }

}