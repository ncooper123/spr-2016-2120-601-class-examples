import java.util.*;

/**
 * CollectionTest.java example from the text.
 */
public class CollectionTest {

    public static void main(String[] args){
        String[] colors = {"MAGENTA","RED","WHITE","BLUE","CYAN"};
        List<String> list = new ArrayList<String>();
        for (String color : colors){
            list.add(color);
        }
        
        String[] removeColors = {"RED","WHITE","BLUE"};
        List<String> removeList = new ArrayList<String>();
        for (String color : removeColors){
            removeList.add(color);
        }
        
        System.out.println("ArrayList: ");
        for (int count = 0; count < list.size(); count++){
            System.out.printf("%s ", list.get(count));
        }
        removeColors(list,removeList);
        System.out.printf("%n%nArrayList after calling removeColors:%n");
        for (String color : list){
            System.out.printf("%s ",color);
        }
    }
    
    private static void removeColors(Collection<String> c1, Collection<String> c2){
        Iterator<String> iterator = c1.iterator();
        while (iterator.hasNext()){
            String thisElement = iterator.next();
            if (c2.contains(thisElement)){
                iterator.remove();
            }
        }
    }

}